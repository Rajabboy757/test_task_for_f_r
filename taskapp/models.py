from django.db import models


class Rassilka(models.Model):
    start_date_time = models.DateTimeField
    text = models.TextField
    # filter =
    end_date_time = models.DateTimeField


class Client(models.Model):
    phone_number = models.PositiveBigIntegerField
    mobile_operator_code = models.PositiveSmallIntegerField
    tag = models.CharField
    timezone = models.CharField


class Message(models.Model):
    created_date_time = models.DateTimeField(auto_created=True)
    sending_status = models.BooleanField(default=False)
    rassilka_id = models.PositiveIntegerField
    client_id = models.PositiveIntegerField
